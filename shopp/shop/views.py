from django.shortcuts import render
from django.http import HttpResponse
from .models import Product , review
from django.contrib.auth.forms import UserCreationForm
from math import ceil


def index(request):
    return render(request,'shop/Home.html')


def store(request):
    products = Product.objects.all()
    n=len(products)
    proinfo={'range':range(1,n),'product':products}
    return render(request,'shop/store.html',proinfo)

# def store(request):
#     products = Product.objects.all()
#     n = len(products)
#
#     slide = (n // 3) + ceil((n / 3) - (n // 3))
#     params = {'no_of_slide': slide, 'range': range(1, slide), 'product': products}
#     return render(request, 'shop/index.html', params)

def about(request):
    return render(request,'shop/about.html')

def contact(request):
    return render(request,'shop/contact.html')

def productview(request,myid):
    products= Product.objects.filter(id=myid)
    parama={'product': products}
    return render(request,'shop/productview.html', parama)

def revi(request):
    rev = request.POST.get('review')
    revie=review(reviews=rev)
    par={'revs':revie}

    return render(request, 'shop/productview.html',par)
def cart(request):
    return render(request,'shop/cart.html')

def chechout(request):
    return HttpResponse("checkout Page")

def signin(request):
    form =UserCreationForm
    return render(request,'shop/signup.html',{'form':form})



def login(request):
    return render(request,'shop/login.html')
