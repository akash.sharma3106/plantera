# Generated by Django 3.0.8 on 2020-08-31 19:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0003_auto_20200828_0442'),
    ]

    operations = [
        migrations.CreateModel(
            name='review',
            fields=[
                ('rev_id', models.AutoField(primary_key=True, serialize=False)),
                ('reviews', models.CharField(max_length=50)),
            ],
        ),
    ]
