from django.urls import path
from . import views
urlpatterns = [
    path("", views.index, name="shopeHome"),
    path("store/",views.store, name="store"),
    path("about/", views.about, name="about"),
    path("contact/", views.contact, name="Contact"),
    path("product/<int:myid>", views.productview, name="productview"),
    path("store/cart/", views.cart, name="cart"),
    path("checkout/", views.chechout, name="checkout"),
    path("sign/", views.signin, name="signin"),
    path("login/", views.login, name="login"),
    path("product/reviews/", views.revi, name="review")

]